
import sys
import mysql.connector
from mysql.connector import Error
#yeeeeaaah! its me marçal!
#Gracias Antu! Alvaro
#Defino la Clase Bus
class Bus:
    def __init__(self, bus_id, Nombre, total_seats, available_seats):
        self.SetNombre(Nombre)
        self.SetID(bus_id)
        self.SetTotal(total_seats)
        self.SetAvailable(available_seats)

    def SetNombre(self, pNombre):
        self.nombre = pNombre

    def SetID(self, bus_id):
        self.id = bus_id

    def SetTotal(self, total_seats):
        self.total_seats = total_seats

    def SetAvailable(self, available_seats):
        self.available_seats = available_seats

    def GetNombre(self):
        return self.nombre

    def GetID(self):
        return self.id

    def GetTotal(self):
        return self.total_seats

    def GetAvailable(self):
        return self.available_seats

#Clase BaseDatos es para conectar con la Base de Datos
class BaseDatos:

    def connectionDB(self):
        try:
            connection = mysql.connector.connect(host='localhost',
                                                 database='bus',
                                                 user='root',
                                                 password='Llàstics%2020')
            if connection.is_connected():
                return connection
        except Error as e:
            print("Error while connecting to MySQL", e)
            return False

    def closeDB(self,cursor, connection):
        cursor.close()
        connection.close()

#Clase Bus_Admin contiene todas las funciones para gestionar los buses y los pasajeros
class Bus_Admin:
    
    basedatos=BaseDatos()

    def __init__(self):
        pass

    def getBus(self):
        try:
            connection = self.basedatos.connectionDB()
            if connection != False:
                cursor = connection.cursor()
                cursor.execute("SELECT * FROM tb_bus")
                bus = cursor.fetchall()
                print('ID,Total,Disponible')
                for i in bus:
                    print(i)
            else:
                self.sortir()
        except Error as e:
            print("Error while fetching data", e)
            self.sortir()
        finally:
            if (connection.is_connected()):
                self.basedatos.closeDB(cursor, connection)

    def getData(self,id):
        try:
            connection = self.basedatos.connectionDB()
            if connection != False:
                cursor = connection.cursor()
                cursor.execute("SELECT * FROM tb_bus WHERE bus_id ="+id)
                bus = cursor.fetchall()[0]
                return bus
            else:
                self.sortir()
        except Error as e:
            print("Error while fetching data", e)
            self.sortir()
        finally:
            if (connection.is_connected()):
                self.basedatos.closeDB(cursor, connection)

    def crearBus(self):
        total_seats = input('¿Cuantas plazas tiene el autobus?\n')
        seats_available = input('¿Cuantas plazas disponibles tiene?\n')
        bus_name = input('¿Cuál es el nombre del bus?\n')
        try:
            connection = self.basedatos.connectionDB()
            if connection != False:
                cursor = connection.cursor()
                sql = "INSERT INTO tb_bus (total_seats,seats_available,bus_name) VALUES (%s,%s,%s)"
                val = (total_seats, seats_available, bus_name)
                cursor.execute(sql, val)
                connection.commit()
                print('Datos actualizados correctamente')
            else:
                self.sortir()
        except Error as e:
            print("Error while updating data", e)
            self.sortir()
        finally:
            if (connection.is_connected()):
                self.basedatos.closeDB(cursor, connection)

    def editDB(self,bus):

        try:
            connection = self.basedatos.connectionDB()
            if connection != False:
                cursor = connection.cursor()
                message = "UPDATE tb_bus SET seats_available = " + \
                    str(bus.GetAvailable())+" WHERE bus_id = "+str(bus.GetID())
                cursor.execute(message)
                connection.commit()
                print('Operación realizada con éxito')
                print(50*'*')
                print('Datos actualizados correctamente')
            else:
                self.sortir()
        except Error as e:
            print("Error while updating data", e)
            self.sortir()
        finally:
            if (connection.is_connected()):
                self.basedatos.closeDB(cursor, connection)
    
    def seleccionarBus(self):
        print('Listado de Buses:')
        self.getBus()
        print('0.-Crear nuevo bus')

    def venda(self,bus):
        print(50*'*')
        print('Funcion de venta')
        reserva = int(input('¿Cuantas plazas desea comprar?\n'))
        if reserva < bus.GetAvailable():
            bus.SetAvailable(bus.GetAvailable()-reserva)
            lista_pasajero=[]
            for i in range(0,reserva):
                nombre=input('Nombre: ')
                apellido=input('Apellido: ')
                direccion=input('Direccion: ')
                identidad=input('DNI/NIE: ')
                persona=Pasajero(nombre,apellido,direccion,identidad,bus.GetID())
                lista_pasajero.append(persona)
            self.CrearPasajero(lista_pasajero)
            
        else:
            print('Error no hay plazas disponibles')
            print(50*'*')

    def devolucio(self,bus):
        print(50*'*')
        print('Funcion de devolucion')
        ventas_total = bus.GetTotal()-bus.GetAvailable()
        devolver = int(input('¿Cuantos billetes desea devolver?\n'))
        if devolver <= ventas_total:
            bus.SetAvailable(bus.GetAvailable()+devolver)
            for i in range(0,devolver):
                pasajero_id=input('Introduce el DNI/NIE del pasajero: ')
            self.EliminarPasajero(pasajero_id,bus)
            print('Devolución realizada con éxito')
            print(50*'*')
        else:
            print('Error: No se pueden devolver tantos billetes')
            print(50*'*')

    def estat(self,bus):
        print(50*'*')
        print('Funcion de estado')
        print('Plazas totales del autobus: ', bus.GetTotal())
        print('Plazas libres: ', bus.GetAvailable())
        print('Plazas vendidas: ', bus.GetTotal()-bus.GetAvailable())
        print(50*'*')

    def sortir(self):
        print(50*'*')
        print('Saliendo del programa....')
        print(50*'*')
        sys.exit()
    
    def CrearPasajero(self,pasajeros):
        try:
            connection = self.basedatos.connectionDB()
            if connection != False:
                for (i,x) in enumerate(pasajeros):
                    pasajero=pasajeros[i]
                    cursor = connection.cursor()
                    sql = "INSERT INTO pasajeros (Nombre,Apellido,Direccion,Dni_Nie,bus_id) VALUES (%s,%s,%s,%s,%s)"
                    val = (pasajero.GetNombre(),pasajero.GetApellido(),
                            pasajero.GetDireccion(),pasajero.GetIdentidad(),pasajero.GetBusId())
                    cursor.execute(sql, val)
                    connection.commit()
                print('Datos actualizados correctamente')
            else:
                self.sortir()
        except Error as e:
            print("Error while updating data", e)
            self.sortir()
        finally:
            if (connection.is_connected()):
                self.basedatos.closeDB(cursor, connection)

    def EliminarPasajero(self,pasajero_id,bus):

        try:
            connection = self.basedatos.connectionDB()
            if connection != False:
                cursor = connection.cursor()
                sql = f"DELETE FROM bus.pasajeros WHERE Dni_Nie = '{pasajero_id}' AND bus_id = {bus.GetID()} "
                cursor.execute(sql)
                connection.commit()
                print('Datos actualizados correctamente')
            else:
                self.sortir()
        except Error as e:
            print("Error while updating data", e)
            self.sortir()
        finally:
            if (connection.is_connected()):
                self.basedatos.closeDB(cursor, connection)

#Hacer los menús
class Interfaz:
    def __init__(self):
        pass
    def MenuBuses(self,admin):
        bus_admin.seleccionarBus()
        id = input('¿Que bus (número) quieres usar?\n')
        try:
            int(id)
        except ValueError:
            print('Debes introducir un número')
        self.MenuBuses(admin)
        while id == '0':
            print('Voy a crear un bus')
            print('Crear Bus:')
            bus_admin.crearBus()
            bus_admin.seleccionarBus()
            id = input('¿Que bus (número) quieres usar?\n')
        return id

#Defino la Clase Pasajero
class Pasajero:
    def __init__(self,Nombre,Apellido,Direccion,Identidad,busId):
        self.SetNombre(Nombre)
        self.SetApellido(Apellido)
        self.SetDireccion(Direccion)
        self.SetIdentidad(Identidad)
        self.SetBusId(busId)
    def SetNombre(self,Nombre):
        self.__nombre=Nombre
    def SetBusId(self,busId):
        self.__busId=busId
    def SetApellido(self,Apellido):
        self.__apellido=Apellido
    def SetDireccion(self,Direccion):
        self.__direccion=Direccion
    def SetIdentidad(self,Identidad):
        self.__identidad=Identidad
    def GetNombre(self):
        return self.__nombre
    def GetApellido(self):
        return self.__apellido
    def GetDireccion(self):
        return self.__direccion
    def GetIdentidad(self):
        return self.__identidad
    def GetBusId(self):
        return self.__busId
    
opcion = 'x'
print('Bienvenido')
print(50*'*')
print('')

#Creamos Objetos para administrar el programa
bus_admin=Bus_Admin()
menus=Interfaz()

#Menu para Seleccionar los buses
id=menus.MenuBuses(bus_admin)

#Menu para Seleccionar las funciones del Bus
while opcion != '0':

    #Creamos nuestro Objeto Bus
    bus = bus_admin.getData(id)
    bus_class = Bus(bus[0], bus[3], bus[1], bus[2])
    
    opcion = input(
        'Seleccione un opción con el número correspondiente:\n1.-Venta de billetes\n2.-Devolución de Billetes\n3.-Estado de la venta\n0.-Salir\n')
    
    #Llamamos Funcion para Vender
    if opcion == '1':

        bus_admin.venda(bus_class)
        bus_admin.editDB(bus_class)

    #Llamamos Funcion para Devolucion
    elif opcion == '2':

        bus_admin.devolucio(bus_class)
        bus_admin.editDB(bus_class)

    #Llamamos Funcion para ver Estado
    elif opcion == '3':
        bus_admin.estat(bus_class)
    
    #Llamamos Funcion para Cerrar Programa
    elif opcion == '0':
        bus_admin.sortir()
    
    #Llamamos Funcion Cerrar para Opcion Incorrecta
    else:
        print('Opción no válida. Saliendo del programa')
        bus_admin.sortir()
